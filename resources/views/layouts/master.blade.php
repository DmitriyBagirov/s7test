<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Самолет летать еда</title>
	<!-- Bootstrap Core CSS -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body id="page-top" class="index">
<div id="app">
	@include('layouts.partials._navigation')
	@include('layouts.partials._header')
	<div class="content">
		<example></example>
	</div>
	@include('layouts.partials._footer')
</div>
</body>
</html>