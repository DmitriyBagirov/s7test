<?php

namespace App\Http\Controllers;

use App\FlightLoad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomePage extends Controller
{

	static function index()
	{
		return view('layouts.master');
	}

	static function loadTable(Request $request)
	{
		$page = is_numeric($request->page) ? $request->page : 1;
		$limit = is_numeric($request->limit) ? $request->limit : 20;
		$filterQuery = isset($request->filterQuery) ? $request->filterQuery : '';
		$orderField = isset($request->orderField) ? $request->orderField : 'fid';
		$direction = isset($request->direction) ? $request->direction : 'asc';
		$skip = ($page - 1) * $limit;
		$flights = self::getTable($skip, $limit, $filterQuery, $orderField, $direction);
		$result = $flights == null ? 1 : 0;
		return compact('flights', 'result');
	}

	static function getTable($skip = 0, $limit = 20, $filterQuery = '', $orderField = 'fid', $direction = 'asc')
	{
		DB::statement("SET SESSION sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
		$query = FlightLoad::selectRaw("
			bm.flight_id fid,
			bm.flight_date fdate,
			bm.type type,
			bm.class class,
			GROUP_CONCAT(DISTINCT mr.iata_code) pcode,
			GROUP_CONCAT(DISTINCT if(bm.iata_code = 'ALC', NULL, bm.iata_code) ORDER BY bm.iata_code) fcode, 
			nm.meal_qty pqty,
			SUM(if(bm.iata_code = 'ALC', NULL, bm.qty)) fqty,
			round(nm.pprice / 1.18, 2) pprice,
			round(SUM(if(bm.iata_code = 'ALC', 0, 1.04166667 * bm.price_per_one * bm.qty)), 2) fprice,
			round(nm.pprice / 1.18 - sum(if(bm.iata_code = 'ALC', 0, 1.04166667 * bm.price_per_one * bm.qty)), 2) delta
			")
			->join('billed_meals as bm', 'bm.flight_id', 'fl.flight_id')
			->join('meal_rules as mr', 'mr.flight_id', 'fl.flight_id')
			->join(DB::raw('
				(select iata_code, passenger_amount, SUM(meal_qty) meal_qty, SUM(meal_qty * price) pprice from new_matrix 
				join business_meal_prices bmp on new_matrix.nomenclature = bmp.nomenclature 
				group by iata_code, passenger_amount) nm 
			'), 'nm.iata_code', 'mr.iata_code')
			->whereRaw('`bm`.`flight_date` = `fl`.`flight_date`')
			->whereRaw('`nm`.`passenger_amount` = `fl`.`business`')
			->where('bm.class', 'Бизнес')
			->where('bm.type', 'Комплект')
			->where('mr.class', 'Бизнес')
			->Year(2017)
			->Month(1)
			->whereRaw('mr.weeknumber = ((WEEK(fl.flight_date, 1) + 1) % 2 + 1)')
			->groupBy(['bm.flight_id', 'bm.flight_date'])
			->orderBy($orderField, $direction)
			->limit($limit)
			->skip($skip);
		$flights = DB::select("select 
 		fid 'Рейс', fdate 'Дата', type 'Тип', class 'Класс', pcode 'Код(план)',
 		 fcode 'Код(факт)', pqty 'Кол-во(план)', fqty 'Кол-во(факт)', 
 		 pprice 'Цена(план)', fprice 'Цена(факт)', delta 'Дельта'
 		from (" . $query->toSql() . ") report
		where report.fid     like '%$filterQuery%' or 
		 report.fdate   like '%$filterQuery%' or 
		 report.type    like '%$filterQuery%' or 
		 report.class   like '%$filterQuery%' or 
		 report.pcode   like '%$filterQuery%' or 
		 report.fcode   like '%$filterQuery%' or 
		 report.pqty    like '%$filterQuery%' or 
		 report.fqty    like '%$filterQuery%' or 
		 report.pprice  like '%$filterQuery%' or 
		 report.fprice  like '%$filterQuery%' or 
		 report.delta   like '%$filterQuery%'
		", $query->getBindings());
		if (sizeof($flights) == 0 ) {
			return null;
		}
		foreach ($flights as $key => $flight) {
			$flights[$key] = json_decode(json_encode($flight), true);
		}
		$fields = ['fid', 'fdate', 'type', 'class', 'pcode', 'fcode', 'pqty', 'fqty', 'pprice', 'fprice', 'delta'];
		$keys = array_keys($flights[0]);
		$fields = array_combine($fields, $keys);
		return compact('flights', 'fields');
	}
}
