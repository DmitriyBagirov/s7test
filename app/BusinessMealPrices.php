<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BusinessMealPrices
 *
 * @property int $id
 * @property string|null $nomenclature
 * @property float|null $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BusinessMealPrices whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BusinessMealPrices whereNomenclature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BusinessMealPrices wherePrice($value)
 * @mixin \Eloquent
 */
class BusinessMealPrices extends Model
{
	public $timestamps = false;
}
