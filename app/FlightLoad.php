<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FlightLoad
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FlightLoad month($month)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FlightLoad year($year)
 * @mixin \Eloquent
 *
 */
class FlightLoad extends Model
{
	public $timestamps = false;
	protected $table = 'flight_load as fl';

	public static function scopeMonth($query, $month) {
		return $query->whereRaw('Month(fl.flight_date) = ' . $month);
	}

	public static function scopeYear($query, $year) {
		return $query->whereRaw('Year(fl.flight_date) = ' . $year);
	}

}
