<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\MealRules
 *
 * @property int $id
 * @property int|null $flight_id
 * @property string|null $class
 * @property int|null $weeknumber
 * @property string|null $iata_code
 * @property string|null $meal_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MealRules whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MealRules whereFlightId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MealRules whereIataCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MealRules whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MealRules whereMealType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MealRules whereWeeknumber($value)
 * @mixin \Eloquent
 */
class MealRules extends Model
{
	public $timestamps = false;
}
