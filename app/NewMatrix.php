<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\NewMatrix
 *
 * @mixin \Eloquent
 */
class NewMatrix extends Model
{
	public $timestamps = false;
}
