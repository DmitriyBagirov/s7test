<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BilledMeals
 *
 * @property int $id
 * @property int|null $flight_id
 * @property int|null $flight_load_id
 * @property string|null $flight_date
 * @property string|null $nomenclature
 * @property string|null $iata_code
 * @property string|null $type
 * @property int|null $delivery_number
 * @property string|null $name
 * @property string|null $class
 * @property string|null $bortnumber
 * @property int|null $qty
 * @property float|null $total
 * @property float|null $total_novat_discounted
 * @property string|null $airport
 * @property int|null $invoice_number
 * @property string|null $name_code
 * @property float|null $price_per_one
 * @property int|null $checked
 * @property string|null $check_date
 * @property string|null $validation_errors
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereAirport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereBortnumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereCheckDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereDeliveryNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereFlightDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereFlightId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereFlightLoadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereIataCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereInvoiceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereNameCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereNomenclature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals wherePricePerOne($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereTotalNovatDiscounted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BilledMeals whereValidationErrors($value)
 * @mixin \Eloquent
 */
class BilledMeals extends Model
{
	public $timestamps = false;
}
